function getCep(value){
    console.log(value)
    fetch(`http://viacep.com.br/ws/${value}/json/`)
        .then( res => res.json())
        .then((address) => {
            console.log(address)
            showAddress(address)
        }).catch(err => console.log(err))
}

showAddress = address => {
    const resposta = document.querySelector('#resposta');

    var rows = "";

    rows += `<p>Logradouro: ${address.logradouro}</p>`
    rows += `<p>Bairro: ${address.bairro}</p>`
    rows += `<p>Cidade: ${address.localidade}</p>`
    rows += `<p>UF: ${address.uf}</p>`
    
    resposta.innerHTML = rows

}